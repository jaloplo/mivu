module.exports = function(grunt) {

	// Project configuration.  
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		clean: {
			build: ['./build/'],
			release: ['./release/<%= pkg.version %>/', './release/mivu.<%= pkg.version %>.zip']
		},
		complexity: {
			build: {
					src: ['./src/*.js'],
					options: {
						breakOnErrors: false,
						jsLintXML: './reports/report.xml',
						checkstyleXML: './reports/checkstyle.xml',
						errorsOnly: false,
						cyclomatic: 5,
						halstead: 15,
						maintainability: 100
					}
				}
		},
		compress: {
			release: {
				options: {
					archive: './release/mivu.<%= pkg.version %>.zip',
					mode: 'zip'
				},
				files: [{
					cwd: './release/<%= pkg.version %>',
					src: ['**/*'],
					expand: true
				}]
			}
		},
		docco: {
			build: {
				src: ['./src/*.js'],
				options: {
					output: './build/doc',
					version: '<%= pkg.version %>',
					layout: 'linear'
				}
			},
			release: {
				src: ['./src/*.js'],
				options: {
					output: './release/<%= pkg.version %>/doc',
					version: '<%= pkg.version %>',
					layout: 'linear'
				}
			}
		},
		qunit: {
			all: ['./test/**/*.html']
		},
		jshint: {
			options: {
				bitwise: true,
				curly: true,
				eqeqeq: true,
				es3: true,
				freeze: true,
				newcap: true,
				strict: true,
				undef: true,
				unused: true,
				globals: {
					Event: false
				}
			},
			files: {
				src: ['./src/*.js']
			}
		},
		uglify: {
			build: {
				files: {
					'./build/mivu.min.js': ['./src/mivu.js'],
					'./build/mivu.effects.min.js': ['./src/mivu.effects.js']
				}
			},
			release: {
				files: {
					'./release/<%= pkg.version %>/mivu.min.js': ['./src/mivu.js'],
					'./release/<%= pkg.version %>/mivu.effects.min.js': ['./src/mivu.effects.js']
				}
			}
		}
	});
  
  grunt.loadNpmTasks('grunt-complexity');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-text-replace');
  
  var docco = require('docco');
  
  grunt.registerMultiTask('docco', 'Docco processor.', function() {
    // Either set the destination in the files block, or (prefferred) in { options: output }
    // var options = this.options();
	// options.output = options.output || (this.file && this.file.dest);
    docco.document(this.options({ args: this.filesSrc }), this.async());
  });

  grunt.registerTask('default', ['clean', 'complexity', 'qunit', 'uglify', 'docco', 'compress']);
  grunt.registerTask('build', ['clean:build', 'complexity', 'qunit', 'uglify:build', 'docco:build']);
  grunt.registerTask('release', ['clean:release', 'complexity', 'qunit', 'uglify:release', 'docco:release', 'compress:release']);
};