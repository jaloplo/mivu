test('Mivu', function() {
	notEqual(undefined, Mivu, 'Mivu is not undefined');
	notEqual(null, Mivu, 'Mivu is not null');
});

test('Mivu.clone', function() {
	notEqual(undefined, Mivu.clone, 'clone is not undefined');
	notEqual(null, Mivu.clone, 'clone is not null');
	
	var source = {
		'id': 1,
		'description': 'source description'
	};
	
	
	// method call
	var target = Mivu.clone(source);
	
	// checking all properties has been cloned to target from source and their values
	notEqual(undefined, target, 'target is not undefined');
	notEqual(null, target, 'target is not null');
	notEqual(undefined, target.id, 'target.id is not undefined');
	notEqual(null, target.id, 'target.id is not null');
	notEqual(undefined, target.description, 'target.description is not undefined');
	notEqual(null, target.description, 'target.description is not null');
	
	equal(target.id, 1, 'id is equal to 1');
	equal(target.description, 'source description', 'description is equal to source description');
	
	// changing target values
	target.id = 2;
	target.description = 'target description';
	target.title = 'target';
	
	// checking all properties has new value
	notEqual(undefined, target.id, 'target.id is not undefined');
	notEqual(null, target.id, 'target.id is not null');
	notEqual(undefined, target.description, 'target.description is not undefined');
	notEqual(null, target.description, 'target.description is not null');
	notEqual(undefined, target.title, 'target.title is not undefined');
	notEqual(null, target.title, 'target.title is not null');
	
	equal(target.id, 2, 'id is equal to 2');
	equal(target.description, 'target description', 'description is equal to target description');
	equal(target.title, 'target', 'description is equal to target');

	// checking source object has not been impacted by changes on target
	notEqual(undefined, source.id, 'source.id is not undefined');
	notEqual(null, source.id, 'source.id is not null');
	notEqual(undefined, source.description, 'source.description is not undefined');
	notEqual(null, source.description, 'source.description is not null');
	equal(undefined, source.title, 'source.title is undefined');
	
	equal(source.id, 1, 'id is equal to 1');
	equal(source.description, 'source description', 'description is equal to source description');
});

test('Mivu.inherit', function() {
	notEqual(undefined, Mivu.inherit, 'inherit is not undefined');
	notEqual(null, Mivu.inherit, 'inherit is not null');
	
	var parent = {
		'id': 1,
		'description': 'parent description',
		'type': 'parent'
	};
	
	notEqual(undefined, parent.id, 'parent.id is not undefined');
	notEqual(null, parent.id, 'parent.id is not null');
	notEqual(undefined, parent.description, 'parent.description is not undefined');
	notEqual(null, parent.description, 'parent.description is not null');
	notEqual(undefined, parent.type, 'parent.type is not undefined');
	notEqual(null, parent.type, 'parent.type is not null');
	
	equal(parent.id, 1, 'parent.id is equal to 1');
	equal(parent.description, 'parent description', 'parent.description is equal to parent description');
	equal(parent.type, 'parent', 'parent.type is equal to parent');
	
	var child = {
		'type': 'child'
	};
	
	equal(undefined, child.id, 'child.id is undefined');
	equal(undefined, child.description, 'child.description is undefined');
	notEqual(undefined, child.type, 'child.type is not undefined');
	notEqual(null, child.type, 'child.type is not null');
	
	equal(child.type, 'child', 'child.type is equal to child');
	
	
	// method call
	Mivu.inherit(child, parent);
	
	
	// parent object hasn't been impacted
	notEqual(undefined, parent.id, 'parent.id is not undefined');
	notEqual(null, parent.id, 'parent.id is not null');
	notEqual(undefined, parent.description, 'parent.description is not undefined');
	notEqual(null, parent.description, 'parent.description is not null');
	notEqual(undefined, parent.type, 'parent.type is not undefined');
	notEqual(null, parent.type, 'parent.type is not null');
	
	equal(parent.id, 1, 'parent.id is equal to 1');
	equal(parent.description, 'parent description', 'parent.description is equal to parent description');
	equal(parent.type, 'parent', 'parent.type is equal to parent');
	
	// child object has new properties
	notEqual(undefined, child.id, 'child.id is not undefined');
	notEqual(null, child.id, 'child.id is not null');
	notEqual(undefined, child.description, 'child.description is not undefined');
	notEqual(null, child.description, 'child.description is not null');
	notEqual(undefined, child.type, 'child.type is not undefined');
	notEqual(null, child.type, 'child.type is not null');
	
	equal(child.id, 1, 'child.id is equal to 1');
	equal(child.description, 'parent description', 'child.description is equal to parent description');
	equal(child.type, 'child', 'child.type is equal to child');
});

test('Mivu.Events', function() {
	notEqual(undefined, Mivu.Events, 'Mivu.Events is not undefined');
	notEqual(null, Mivu.Events, 'Mivu.Events is not null');
});

// test('Mivu.Events functions', function() {
	// notEqual(undefined, Mivu.Events.fire, 'fire is not undefined');
	// notEqual(null, Mivu.Events.fire, 'fire is not null');
	// notEqual(undefined, Mivu.Events.off, 'off is not undefined');
	// notEqual(null, Mivu.Events.off, 'off is not null');
	// notEqual(undefined, Mivu.Events.on, 'on is not undefined');
	// notEqual(null, Mivu.Events.on, 'on is not null');
	// notEqual(undefined, Mivu.Events.remove, 'remove is not undefined');
	// notEqual(null, Mivu.Events.remove, 'remove is not null');
	
	// // This is the object to be tested.
	// var counter = (function(){
		
		// var current = 0;
		
		// var that = {
			// get: function() {
				// return current;
			// },
			
			// increment: function() {
				// Mivu.Events.fire(this, 'incrementing');
				// var oldValue = current;
				// current = current + 1;
				// var newValue = current;
				// Mivu.Events.fire(this, 'incremented', { 
					// oldValue: oldValue, 
					// newValue: newValue
				// });
			// }
		// };
		
		// return that;
	// })();
	
	
	// // Setting the trigger to check if fire works and on works
	// var flagIncrementing = 0;
	// var flagIncremented = 0;
	// var oldValue = 0;
	// var newValue = 0;
	
	// Mivu.Events.on(counter, 'incrementing', function() {
		// flagIncrementing = flagIncrementing + 1;
	// });
	
	// function _incremented(event) {
		// flagIncremented = flagIncremented + 1;
		// oldValue = event.oldValue;
		// newValue = event.newValue;
	// }
	
	// Mivu.Events.on(counter, 'incremented', _incremented);
	
	// equal(counter.get(), 0, 'counter initial value is 0');
	// equal(flagIncrementing, 0, 'flag for incrementing event is 0');
	// equal(flagIncremented, 0, 'flag for incremented event is 0');
	// equal(oldValue, 0);
	// equal(newValue, 0);
	
	// counter.increment();
	
	// equal(counter.get(), 1, 'counter value is 1');
	// equal(flagIncrementing, 1, 'flag for incrementing event is 1');
	// equal(flagIncremented, 1, 'flag for incremented event is 1');
	// equal(oldValue, 0);
	// equal(newValue, 1);
	
	// counter.increment();
	
	// equal(counter.get(), 2, 'counter value is 2');
	// equal(flagIncrementing, 2, 'flag for incrementing event is 2');
	// equal(flagIncremented, 2, 'flag for incremented event is 2');
	// equal(oldValue, 1);
	// equal(newValue, 2);
	
	
	// // checking if off works
	// Mivu.Events.off(counter, 'incremented', _incremented);
	
	// counter.increment();
	
	// equal(counter.get(), 3, 'counter value is 3');
	// equal(flagIncrementing, 3, 'flag for incrementing event is 3');
	// equal(flagIncremented, 2, 'flag for incremented event is 2');
	// equal(oldValue, 1);
	// equal(newValue, 2);
	
	
	// // checking if remove works
	// Mivu.Events.remove(counter, 'incrementing');
	
	// counter.increment();
	
	// equal(counter.get(), 4, 'counter value is 4');
	// equal(flagIncrementing, 3, 'flag for incrementing event is 3');
	// equal(flagIncremented, 2, 'flag for incremented event is 2');
	// equal(oldValue, 1);
	// equal(newValue, 2);
// });