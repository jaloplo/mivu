# Mivu Library

**Mivu** is a library which aims to learn best practices on javascript code.
The code is based on *Douglas Crockford* learnings and his book [JavaScript:
The Good Parts](http://www.amazon.com/JavaScript-Good-Parts-Douglas-Crockford/dp/0596517742).

The current *features* of **Mivu** are:

 + Simple object *cloning* to make copies easier.
 + Simple object *inheriting* to create objects hierarchies.
 + *Mvc pattern* for making javascript client applications.

The project features are:

 + For *testing* purposes it is used [QUnit](https://qunitjs.com/).
 + For *building* purposes it is used [Grunt](http://gruntjs.com/).
 + For *documentation* purposes it is used [Docco](http://jashkenas.github.io/docco/).
 + For *code quality* purposes it is used [JSHint](http://www.jshint.com/).
 + For *code analysis* purposes it is used [JSComplexity](http://jscomplexity.org/).
 
 You can *download* the lastest stable version of the library [**here**](https://bitbucket.org/jaloplo/mivu/downloads/mivu.min.1.1.1.js).
