// # Mivu effects Library
// ----------------------
// **Mivu Effects** is a library with html effects to be used on your
// pages. As [**Mivu**](https://bitbucket.org/jaloplo/mivu) library, it aims to learn best
// practices on javascript code. The code is based on Douglas Crockford learnings and his
// book [JavaScript: The Good Parts](http://www.amazon.com/JavaScript-Good-Parts-Douglas-Crockford/dp/0596517742).
// The current effects implemented are:
//  + *Fade In*: Makes appear something.
//  + *Fade Out*: Makes disappear something.

// *JSHint flags*
/* jshint bitwise: true */ /* prohibition of bitwise operators */
/* jshint curly: true */ /* curly braces have to be put always on loops and conditionals */
/* jshint eqeqeq: true */ /* prohibition of == != */
/* jshint es3: true */ /* ECMAScript 3 specification */
/* jshint freeze: true */ /* prohibition of overwritting prototypes of native objects */
/* jshint newcap: true */ /* capitalization of constructor functions */
/* jshint strict: true */ /* ECMAScript 5 strict mode */
/* jshint undef: true */ /* prohibition of undeclared variables */
/* jshint unused: true */ /* warning about unused variables */

/* global Mivu: false */
/* global window: false */

// ## Mivu.Effects Class
// Mivu.Effects is an static class that exposes the methods to be used.

Mivu.Effects = (function(){
	'use strict';
			
	var steps = 40;
	
	// **fadeIn**
	// This method makes appear an element in a web page. `elem` parameter is mandatory
	// and specifies the dom element to make it appear. `time`parameter is the duration
	// miliseconds of the effect. `callback` is a method to be executed when the effect
	// finalized.
	function _fadeIn(elem, time, callback) {
		time = time || 1000;
		var intervalId = null;
		var opacity = parseFloat(elem.style.opacity) || 0;
		elem.style.display = 'inherit';
		
		intervalId = window.setInterval(function() {
			opacity = opacity + (1/steps);
			elem.style.opacity = opacity;
			if(opacity >= 1) {
				elem.style.opacity = 1;
				window.clearInterval(intervalId);
				if(callback) {
					callback();
				}
			}
		}, (time/steps));
	}
	
	// **fadeOut**
	// This method makes disappear an element in a web page. `elem` parameter is mandatory
	// and specifies the dom element to make it disappear. `time`parameter is the duration
	// miliseconds of the effect. `callback` is a method to be executed when the effect
	// finalized.
	function _fadeOut(elem, time, callback) {
		time = time || 1000;
		var intervalId = null;
		var opacity = parseFloat(elem.style.opacity) || 1;
		intervalId = window.setInterval(function() {
			opacity = opacity - (1/steps);
			elem.style.opacity = opacity;
			if(opacity <= 0) {
				elem.style.opacity = 0;
				window.clearInterval(intervalId);
				elem.style.display = 'none';
				if(callback) {
					callback();
				}
			}
		}, (time/steps));
	}
	
	// The closure object returned by the function.
	return {
		fadeIn: _fadeIn,
		fadeOut: _fadeOut
	};
	
})();